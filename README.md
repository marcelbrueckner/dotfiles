# dotfiles

.files

## Setup new Macs

Run `init.sh` to install everything.

### Brewfile

Make sure [Homebrew](https://brew.sh) is installed. Navigate to the directory the `Brewfile` is located and run:

```bash
brew update
brew bundle
```
