#!/usr/bin/env bash

# Make sure everything is installed

# Homebrew
# The missing package manager for macOS
# https://brew.sh
if ! command -v brew &> /dev/null; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

brew update
brew bundle