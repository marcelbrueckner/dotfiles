# Brewfile
# https://github.com/Homebrew/homebrew-bundle
#


# Dependencies and command-line tools
#

# Homebrew-autoupdate
# An easy, convenient way to automatically update Homebrew.
# https://github.com/DomT4/homebrew-autoupdate
tap "domt4/autoupdate"

# Cowsay
# Cowsay provides configurable talking cows at the terminal or other text presentation location.
# http://cowsay.diamonds/
# https://github.com/cowsay-org/cowsay
tap "cowsay-org/cowsay"
brew "cowsay-apj"

# GNU core utilities
# Basic file, shell and text manipulation utilities
# https://formulae.brew.sh/formula/coreutils
brew "coreutils"


# macOS applications
#

# Homebrew Cask
# "To install, drag this icon ..." no more!
# https://github.com/Homebrew/homebrew-cask
tap "homebrew/cask"

# iTerm2
# iTerm2 is a terminal emulator for macOS that does amazing things
# https://www.iterm2.com/
cask "iterm2"

# Change the font to 14pt Source Code Pro Lite
# https://sourabhbajaj.com/mac-setup/iTerm/
tap "homebrew/cask-fonts"
cask "font-source-code-pro"

# Google Chrome
# https://www.google.com/chrome/
#
# Be aware of some plugins not working as expected (mostly password manager plugins)
# https://github.com/Homebrew/homebrew-cask/issues/6998
cask "google-chrome" unless system "mdfind \"kMDItemKind == 'Application'\" | grep -q 'Google Chrome'"

# Visual Studio Code
# Code editing. Redefined.
# https://github.com/microsoft/vscode
cask "visual-studio-code"

# Sublime Text 3
# Sophisticated text editor for code, markup and prose
# https://www.sublimetext.com/3
cask "sublime-text"

# Docker Desktop for Mac
# Build and run containerized applications and microservices
# https://www.docker.com/products/docker-desktop
cask "docker"

# Spotify
# Listen to new music, podcasts, and songs
cask "spotify"

# Captain
# Manage Docker containers. Instantly from the menu bar.
# https://getcaptain.co/
cask "captain"

# TablePlus
# Database management made easy
# https://tableplus.com/
cask "tableplus"

# PhotoSync Companion
# Transfer and backup photos & videos
# https://www.photosync-app.com/downloads.html
cask "photosync"

# IINA
# The modern media player for macOS.
# https://iina.io
cask "iina"


# Homebrew Driver Casks
# https://github.com/Homebrew/homebrew-cask-drivers
tap "homebrew/cask-drivers"

# Sonos S2
# 
# https://www.sonos.com/en-us/controller-app
cask "sonos"


# Mac App Store applications
#

# mas-cli
# A simple command line interface for the Mac App Store
# https://github.com/mas-cli/mas
brew "mas"

# Paste - Clipboard Manager
# Copy and paste smarter
# Don't forget to install Paste Helper for Direct Paste
# https://apps.apple.com/de/app/paste-clipboard-manager/id967805235
# https://pasteapp.io/helper/
mas "Paste - Clipboard Manager", id: 967805235

# Magnet
# Organize your workspace
# https://apps.apple.com/de/app/magnet/id441258766
mas "Magnet", id: 441258766

# Amphetamine
# Powerful keep-awake utility
# https://apps.apple.com/de/app/amphetamine/id937984704
mas "Amphetamine", id: 937984704

# Telegram
# A new era of messaging
# https://apps.apple.com/us/app/telegram/id747648890
mas "Telegram", id: 747648890

# Affinity Photo
# Professional photo editing
# https://apps.apple.com/us/app/affinity-photo/id824183456
mas "Affinity Photo", id: 824183456

# Todoist
# Reminders, day planner & habit
mas "Todoist", id: 585829637


# Whalebrew images
#

# Whalebrew
# It's like Homebrew, but with Docker images.
brew "whalebrew"
